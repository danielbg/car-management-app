<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'year',
        'model',
        'price',
        'user_id',
        'brand_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function brand() {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
}
