<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Subscription;
use App\Notifications\CarPerBrandNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\DB;
use App\Models\Car;

class SendBrandReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'brand_report:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send count report by brand';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::debug("Send count report by brand...");

        $subscriptors = Subscription::all();

        $data = ['count' => 99];

        $data = Car::select('brand_id', DB::raw('count(*) as conteo'))
            ->groupBy('brand_id')
            ->orderBy('conteo', 'DESC')
            ->get() ;

        foreach($subscriptors as $subscriptor) {
            Log::debug("Email: " . $subscriptor->email);

            Notification::send($subscriptor, new CarPerBrandNotification($data));
        }

        return Command::SUCCESS;
    }
}
