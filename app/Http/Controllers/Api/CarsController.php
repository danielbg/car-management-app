<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Car;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\CarsResource;

class CarsController extends Controller
{
    public function index() {
        return CarsResource::collection(Cache::remember('cars', 10, function() {
            return Car::all();
        }));
    }

    public function show($id) {
        return Cache::remember("car_id_$id", 10, function() use ($id) {
            try {
                return Car::findOrFail($id);
            } catch(\Exception $exception) {
                return json_encode([
                    'status'            => 1,
                    'message'           => 'No se encuentra el registro',
                    'exception_message' => $exception->getMessage()
                ]);
            }
        });
    }
}
