<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;
use Illuminate\Support\Facades\Cache;

class BrandsController extends Controller
{
    public function index(){
        return Cache::remember('brands', (12*60), function(){
            return Brand::all();
        });
    }
}
