<?php

namespace App\Http\Controllers;

use App\Events\CarCountEvent;
use App\Jobs\ReportDeletedCarJob;
use Illuminate\Http\Request;
use App\Models\Car;
use App\Models\Brand;
use App\Models\User;
use App\Models\Subscription;
use App\Notifications\NewCarNotificationEmail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Carbon;
class CarController extends Controller
{
    public function index(Request $request) {
        $cars = Car::paginate(10);
        return view('car.index', ['cars' => $cars]);
    }

    public function create() {
        $brands = Brand::orderBy('name')->get();
        return view('car.create', [
            'formTitle' => 'New car record',
            'brands'    => $brands
        ]);
    }

    public function edit(Request $request, $id){
        Log::debug("Cars edit form");
        try {
            $car = Car::findOrFail($id);
            $brands = Brand::orderBy('name')->get();
            return view('car.create', [
                'formTitle' => 'Edit car record',
                'brands'    => $brands,
                'car'       => $car
            ]);
        } catch(\Exception $exception) {
            return redirect('/cars')->with('message', 'Car not found!');
        }
    }

    public function store(Request $request)
    {
        $formFields = $request->validate([
            'name' => 'required',
            'year' => 'required|integer|between:1910,2022',
            'model' => 'required',
            'price' => 'required|integer|between:0,10000000',
            'brand_id' => 'required',
        ]);

        $formFields['user_id'] = auth()->id();

        $car = Car::create($formFields);

        Log::debug("Notifica que un auto ha sido creado...");

        $adminUser = User::find(1)->first();

        Notification::send($adminUser, new NewCarNotificationEmail($car));

        return redirect('/cars')->with('message', 'El automóvil ha sido creado');
    }

    public function update(Request $request)
    {
        $formFields = $request->validate([
            'name' => 'required',
            'year' => 'required|integer|between:1910,2022',
            'model' => 'required',
            'price' => 'required|integer|between:0,10000000',
            'brand_id' => 'required',
        ]);

        $formFields['user_id'] = auth()->id();

        $car = Car::where('id', $request->id)->first();

        if ($car->user_id != auth()->id())
        {
            abort(403, "Unauthorized action. Don't touch someone else's records");
        }

        $formFields['id'] = $car->id;

        $car->update($formFields);

        return redirect('/cars')->with('message', 'Record updated succesfully');
    }

    public function destroy($id)
    {
        Log::debug("Car destroy $id");

        $car = Car::findOrFail($id);

        if ($car->user_id != auth()->id())
        {
            abort(403, "Unauthorized action. Don't touch someone else's records!");
        }

        //$deletedCar = $car;

        $car->delete();

        // Send email to all emails in subscriptions table when a Car is deleted
        $arrayOfSubscriptorsEmail = Subscription::select('email')->get()->toArray();

        foreach($arrayOfSubscriptorsEmail as $subscriptor) {
            Log::debug("(1) Report: " . $subscriptor['email']);

            ReportDeletedCarJob::dispatch($subscriptor['email']);
        }

        return redirect('/cars')->with('message', 'Record deleted successfully');
    }

    public function carCountReport()
    {
        $carCount = Car::count();

        $dateTime = Carbon::now()->format('Y-m-d - H:i:s');

        // Lanza el evento
        event(new CarCountEvent($carCount, $dateTime));

        return view('car.carCountReport', [
            'carCount' => $carCount,
            'dateTime' => $dateTime
        ]);
    }
}
