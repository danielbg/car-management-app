<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Car;
use Illuminate\Support\Facades\Log;

class NewCarNotificationEmail extends Notification implements ShouldQueue
{
    use Queueable;

    public $car;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct($car)
    {
        $this->car = $car;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Dear ' . $notifiable->name . ' this is a notification to let you know that a new car has been registered:')
            ->line('New car\'s record ID:' . $this->car->id)
            ->line('Name:' . $this->car->name)
            ->line('Year:' . $this->car->year)
            ->line('Model:' . $this->car->model)
            ->line('Created by:' . $this->car->user->name)
            ->line('Brand:' . $this->car->brand->name)
            ->line('Creation date:' . $this->car->created_at);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
