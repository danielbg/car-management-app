<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });

        // Get all Cars (Max 10 times per minute)


        RateLimiter::for('all_cars_limiter', function(Request $request) {
            return Limit::perMinute(3)->response(function()
            {
                $seconds = RateLimiter::availableIn('all_cars_limiter');
                return response("Too many attempts. Try again after " . $seconds . " seconds.", 429);
            });
        });

        // Get a single car by Id (Max 2 times per minute)
        RateLimiter::for('single_car_byId', function(Request $request){
            return Limit::perMinute(2)->response(function(){
                return response("Solo permit 2 intentos por minuto", 429);
            });
        });

        // Get all brands (Max 25 times per hour)
        RateLimiter::for('all_brands', function(Request $request){
            return Limit::perHour(25)->response(function(){
                return response("Solo permite 25 intentos por hora", 429);
            });
        });

    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }
}
