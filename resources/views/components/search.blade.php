<div>
    <form action="/movies">
        <div class="row g-3 align-items-center">
            <div class="col-auto">
                <input type="text" name="search" class="col-4 form-control" aria-describedby="search_help_inline"/>
            </div>

            <div class="col-auto">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </form>
    <div class="col-auto">
        <span id="search_help_inline" class="form-text">Ingresa terminos de búsqueda</span>
    </div>
</div>
