@props(['Movie'])
<div>
    {{-- Cambios de la práctica #6 --}}
    <h2>
        <a href="/movies/{{$Movie['id']}}">
            Movie card for: {{$Movie['Title']}} 
        </a>
    </h2>

    <img class="img-thumbnail" width="200px" src="{{ $Movie->Logo ? asset('storage/' . $Movie['Logo']) : asset('storage/no-image.png') }}"/>

    
    <h3>
        Year: <a href="/movies?year={{$Movie['Year']}}"> {{$Movie['Year']}} </a>
    </h3>
    <h3>
        Genre: <a href="/movies?genre={{$Movie['Genre']}}"> {{$Movie['Genre']}} </a>
    </h3>
    <h3>
        Director: <a href="/movies?director={{$Movie['Director']}}"> {{$Movie['Director']}} </a>
    </h3>
    <h3>
        Producer: {{$Movie['Producer']}}
    </h3>
    <h3>
        Actors: {{$Movie['Actors']}}
    </h3>
    <p>
        {{$Movie['Description']}}
    </p>
</div>