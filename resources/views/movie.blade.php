@extends('layout')

@section('content')
    <div>
        <?php $movie = $Movie; ?>
        <h1>Movie: {{ $movie['Title'] }} ({{ $movie['Year'] }}) Z!</h1>
        <hr>
        <ul>
            <li>Genre: {{ $movie['Genre'] }}</li>
            <li>Director: {{ $movie['Director'] }}</li>
            <li>Producer: {{ $movie['Producer'] }}</li>
            <li>Actors: {{ $movie['Actors'] }}</li>
        </ul>

        <br>
        <a href="/movies/{{ $Movie->id }}/edit">Editar el registro</a>

        <br>
        <form method="POST" action="/movies/{{$Movie->id}}">
            @csrf
            @method('DELETE')
            <button type="submit">Delete</button>
        </form>

        <br>
        <a href="{{ url('/movies') }}">[ Regresar ]</a>
    </div>
@endsection