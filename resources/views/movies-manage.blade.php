@extends('layout')

@section('content')
    <a href="/movies/create">Create new movie</a>
    <br><br>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Title</th>
                <th scope="col">Year</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach($Movies as $Movie)
                <tr>
                    <th scope="row">{{ $Movie->id }}</th>
                    <td>{{ $Movie['Title'] }}</td>
                    <td>{{ $Movie['Year'] }}</td>
                    <td>
                        <a href="/movies/{{ $Movie->id }}">
                            <br>
                            <form method="POST" action="/movies/{{ $Movie->id }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit">Deletear</button>
                            </form>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
