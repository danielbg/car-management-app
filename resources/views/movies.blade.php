@extends('layout')

@section('content')
    <h1>T: {{$title}}</h1>

    @if(session('message'))
        <div>{{ session('message') }}</div>
    @endif
        
    <a href="{{ route('new-movie') }}">[ Agregar un nuevo registro ]</a>

    <br><br>

    {{ $movies->links("pagination::bootstrap-5") }}

    <br><br>

    {{-- Cambios en la practica 07 --}}
    <x-search />

    <br>

    <div class="container">
        <div class="row">
            @foreach($movies as $Movie)
                <div class="col-4">
                    <x-movie-card :Movie="$Movie"/>
                </div>
            @endforeach
        </div>
    </div>

@endsection
