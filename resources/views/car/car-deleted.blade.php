<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Deleted record</title>
    </head>
    <body>
        <h1>A record has been deleted</h1>

        <p>
            This is a notification to let you know that a car record has been deleted.
        </p>
    </body>
</html>
