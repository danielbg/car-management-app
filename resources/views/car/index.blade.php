@extends('layout')

@section('content')
    <!-- Car list -->
    <div class="container">

        <h1 class="h1">Cars</h1>

        @if(auth()->user())
            <a href="{{ url('/cars/create') }}"><button type="button" class="btn btn-primary p-1">
                &nbsp;&nbsp;&nbsp;&nbsp;New&nbsp;&nbsp;&nbsp;&nbsp;
            </button></a>

            &nbsp;&nbsp;&nbsp;

            <a href="{{ url('/cars/carCountReport') }}"><button type="button" class="btn btn-secondary p-1">
                &nbsp;&nbsp;&nbsp;&nbsp;Report car count&nbsp;&nbsp;&nbsp;&nbsp;
            </button></a>
        @endif

        @if(session('message'))
            <div>{{ session('message') }}</div>
        @endif

        {{ $cars->links("pagination::bootstrap-5") }}

        <table class="table">
            <thead>
                <th>Id</th>
                <th>Name</th>
                <th>Model</th>
                <th>Brand</th>
                <th>Year</th>
                <th>Price</th>
                @if(auth()->user())
                    <th>Actions</th>
                @endif
            </thead>
            <tbody>
                @foreach($cars as $car)
                <tr>
                    <td>{{ $car->id }}</td>
                    <td>{{ $car->name }}</td>
                    <td>{{ $car->model }}</td>
                    <td>{{ $car->brand->name }}</td>
                    <td>{{ $car->year }}</td>
                    <td>{{ $car->price }}</td>
                    @if(auth()->user())
                    <td>
                        <a href="{{ url('/cars/edit', ['id' => $car->id]) }}">
                            <button type="button" class="btn btn-warning py-0">
                                Edit
                            </button>
                        </a>

                        <a href="/cars-delete/{{$car->id}}">
                            <form method="GET" action="/cars-delete/{{$car->id}}">
                                @csrf
                                <button type="button" class="btn btn-danger py-0">
                                    Delete
                                </button>
                            </form>
                        </a>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $cars->links("pagination::bootstrap-5") }}

    </div>

@endsection
