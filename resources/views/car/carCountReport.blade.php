@extends('layout')

@section('content')

<div class="container">
    <h1 class="text-center">Car count report</h1>

    <p class="text-center">
        <strong>{{ $carCount }}</strong> cars have been registered ({{ $dateTime }})
        <br>
        This report has been send to the system administrator.
    </p>

    <p class="text-center">
    <a href="{{ url('/cars') }}">Return to car list</a></p>
</div>


@endsection
