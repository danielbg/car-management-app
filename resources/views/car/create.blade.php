@extends('layout')

@section('content')
    <div class="container">
        <h1>{{ $formTitle }}</h1>
        <br>
        <form method="POST"
            @if(isset($car) && !is_null($car))
                action="/cars/update"
            @else
                action="/cars/store"
            @endif>

            @csrf

            @if(isset($car) && !is_null($car))
                <input type="hidden" name="id" id="id" value="{{ $car->id }}">
            @endif

            <div class="form-group">
                <label for="name">Name <span style="color:red">(required)</span></label>
                <input type="text"
                    name="name"
                    id="name"
                    class="form-control"
                    @if(isset($car) && !is_null($car))
                        value="{{$car->name}}"
                    @else
                        value="{{ old('name') }}"
                    @endif
                >
                @error('name')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br>
            <div class="form-group">
                <label for="year">Year <span style="color:red">(required)</span></label>
                <input type="text" name="year" id="year"
                    class="form-control"
                    @if(isset($car) && !is_null($car))
                        value="{{$car->year}}"
                    @else
                        value="{{ old('year') }}"
                    @endif>
                @error('year')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br>
            <div class="form-group">
                <label for="model">Model <span style="color:red">(required)</span></label>
                <input type="text" name="model" id="model"
                    class="form-control"
                    @if(isset($car) && !is_null($car))
                        value="{{ $car->model }}"
                    @else
                        value="{{ old('model') }}"
                    @endif>
                @error('model')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br>
            <div class="form-group">
                <label for="price">Price <span style="color:red">(required)</span></label>
                <input type="text" name="price" id="price"
                    class="form-control"
                    @if(isset($car) && !is_null($car))
                        value="{{ $car->price }}"
                    @else
                        value="{{ old('price') }}"
                    @endif>

                @error('price')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br>
            <div class="form-group">
                <label for="brand_id">
                    Brand <span style="color:red">(required)</span>
                </label>
                <select name="brand_id" id="brand_id" class="form-control">
                    <option value="">-- Select --</option>
                    @foreach($brands as $brand)
                        @if(isset($car) && !is_null($car) && ($brand->id == $car->brand_id))
                            <option value="{{ $brand->id }}" selected>{{$brand->name}}</option>
                        @else
                            <option value="{{ $brand->id }}">{{$brand->name}}</option>
                        @endif
                    @endforeach
                </select>
                @error('price')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br>
            <br>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">
                    &nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="{{ url('/cars') }}">
                    <button type="button" class="btn btn-link">
                    [ Return to the car list ]
                </button>
                </a>
            <div>
        </form>
    </div>
@endsection
