@extends('layout')

@section('content')
    <div>
        <h1 class="h1">Agregar un nuevo registro</h1>
        <br/>
        <form method="POST" action="/movies" enctype="multipart/form-data">
            @csrf
            <div>
                <label>Title</span>
                <input type="text" name="title" value="{{ @old('title') }}"/>
                @error('title')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br />
            <div>
                <span>Year</span>
                <input type="text" name="year" value="{{ old('year') }}"/>
                @error('year')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br />
            <div>
                <span>Genre</span>
                <input type="text" name="genre" value="{{ old('genre') }}"/>
                @error('genre')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br />
            <div>
                <span>Director</span>
                <input type="text" name="director" value="{{ old('director') }}" />
                @error('director')
                    <span style="color: red">{{ $message }}</span>
                @enderror
            </div>
            <br />
            <div>
                <span>Producer</span>
                <input type="text" name="producer" value="{{ old('producer') }}"/>
                @error('producer')
                    <span style="color: red">{{ $message }}</span>
                @enderror            
            </div>
            <br />
            <div>
                <span>Actors</span>
                <input type="text" name="actors" value="{{ old('actors') }}"/>
                @error('actors')
                    <span style="color: red">{{ $message }}</span>
                @enderror            
            </div>
            <br />
            <div>
                <span>Description</span>
                <textarea rows="5" cols="40" name="description" ></textarea>
            </div>
            <br />

            <!-- File input -->
            <div>
                <span>Logo</span>
                <input type="file" name="Logo">
                @error('logo')
                    <span style="color: red;">{{ $message }}</span>
                @enderror
            </div>

            <!-- Submit -->
            <div>
                <button type="submit">Guardar</button>
            </div>
        </form>        
    </div>
    <a href="{{ route('movie-list') }}">[ Regresar al listado de registros ]</a>
@endsection