@extends('layout')

@section('content')
    <h1>Edit movie</h1>

    <br>

    <form method="POST" action="/movies/{{$Movie->id}}"
        enctype="multipart/form-data">

    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Title</label>
        <input class="form-control" type="text" name="title" value="{{ $Movie->Title}}">
        @error('title')
            <span style="color:red">{{ $message }}</span>
        @enderror

        <label class="form-label">Year</label>
        <input type="text" name="year" value="{{ $Movie->Year }}">
        @error('year')
            <span style="color:red">{{ $message }}</span>
        @enderror

        <label class="form-label">Genre</label>
        <input type="text" name="genre" value="{{ $Movie->Genre }}">
        @error('genre')
            <span style="color:red">{{ $message }}</span>
        @enderror

        <label class="form-label">Director</label>
        <input type="text" name="director" value="{{ $Movie->Director }}">
        @error('director')
            <span style="color:red">{{ $message }}</span>
        @enderror

        <label class="form-label">Producer (*)</label>
        <input type="text" name="producer" value="{{ $Movie->Producer }}">
        @error('producer')
            <span style="color:red">{{ $message }}</span>
        @enderror

        <label class="form-label">Actors (*)</label>
        <input type="text" name="actors" value="{{ $Movie->Actors }}">
        @error('actors')
            <span style="color:red">{{ $message }}</span>
        @enderror

        <label class="form-label">Description</label>
        <textarea name="description" rows="4" cols="40">
            {{ $Movie->Description }}
        </textarea>

        <!-- Aun no jala lo de editar las imagenes -->
        <label class="form-label">Logo</label>
        <input type="file" name="logo">
        <img class="img-thumbnail" src="{{ $Movie->Logo ? asset('storage/' . $Movie['Logo']) : asset('/images/no-image.png') }}"/>

        @error('logo')
            <span style="color: red">{{ $message }}</span>
        @enderror
        
        <div class="form-group">
            <button class="primary" type="submit">Actualizar</button>
        </div>
    </div>

@endsection