<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarController;
use App\Http\Controllers\MoviesController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect(url('/cars'));
});


Route::get('/register', [UserController::class, 'create'])->middleware('guest');

Route::post('/users', [UserController::class, 'store']);

Route::post('/logout', [UserController::class, 'logout'])->middleware('auth');

Route::get('/login', [UserController::class, 'login'])->middleware('guest');

Route::post('/users/authenticate', [UserController::class, 'authenticate']);

// Cars
Route::get('/cars', [CarController::class, 'index'])->name('car-index');

Route::get('/cars/create', [CarController::class, 'create'])->name('car-create');

Route::post('/cars/store', [CarController::class, 'store'])->name('car-store');

Route::get('/cars/edit/{id}', [CarController::class, 'edit'] );

Route::post('/cars/update', [CarController::class, 'update']);

Route::get('/cars-delete/{id}', [CarController::class, 'destroy'])->middleware('auth');

Auth::routes();

Route::get('/home', [CarController::class, 'index'])->name('car-index');

Route::get('/cars/carCountReport', [CarController::class, 'carCountReport']);
