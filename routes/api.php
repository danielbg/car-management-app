<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CarsController;
use App\Http\Controllers\Api\BrandsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Get all cars
//Route::get('/cars', [CarsController::class, 'index'])->middleware('throttle: 5, 1');
Route::get('/cars', [CarsController::class, 'index'])->middleware('throttle:all_cars_limiter');
//Route::get('/cars', [CarsController::class, 'index']);

// Get single car by id
Route::get('/cars/{id}', [CarsController::class, 'show'])->middleware('throttle:single_car_byId');

// Get all brands
Route::get('/brands', [BrandsController::class, 'index'])->middleware('throttle:all_brands');
