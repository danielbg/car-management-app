<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Movie>
 */
class MovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'Title'       => $this->faker->sentence(),
            'Genre'       => Str::random(10),
            'Director'    => $this->faker->name(),
            'Producer'    => $this->faker->name(),
            'Actors'      => $this->faker->name() . ', ' . $this->faker->name(). ', ' . $this->faker->name(),
            'Year' => $this->faker->numberBetween(1910, 2022),
            'Description' => $this->faker->paragraph(5),
        ];
    }
}
