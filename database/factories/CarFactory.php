<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\Fakecar;
use App\Models\Brand;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $this->faker->addProvider(new Fakecar($this->faker));

        $vehicle = $this->faker->vehicleArray();

        $brand = Brand::inRandomOrder()->first()->toArray();

        $record = [
            'name' => $this->faker->vehicle,
            'year' => $this->faker->numberBetween(1930, 2021),
            'model' => $vehicle['model'],
            'price' => $this->faker->numberBetween(250000, 1200000),
            'brand_id' => $brand['id'],
        ];

        return $record;
    }
}
