<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
//use App\Models\Movie;
use App\Models\Brand;
use App\Models\Car;
use App\Models\Subscription;

use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name'     => 'Administrador',
            'email'    => 'admin@fakemail.com',
            'password' => Hash::make('password'),
            'api_token'=> Str::random(80),
            'is_admin' => true,
        ]);

        $user = User::factory()->create([
            'name'     => 'Juan Perez',
            'email'    => 'juanp@fakemail.com',
            'password' => Hash::make('password'),
            'api_token'=> Str::random(80),
            'is_admin' => false,
        ]);

        Brand::factory(230)->create();

        Car::factory(30)->create(['user_id' => $user->id]);

        Subscription::factory(5)->create();

    }
}
