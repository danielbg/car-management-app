# Proyecto del curso de Laravel del proveedor Knowledge Hut
---

## Datos del participante

- Nombre: Luis Daniel Barajas González
- Email: ldanielbg@comunidad.unam.mx

## Instalación del proyecto

- Crea una base de datos vacía para el proyecto.
- Clona el repositorio en un directorio vacío.
- En la línea de comandos y desde el directorio del proyecto ejecuta `composer install`
- Copiar el archivo `.env.example` al archivo `.env.`
- En la línea de comandos y desde el directorio del proyecto ejecuta `php artisan key:generate` 
- Configura el acceso a la base de datos en el archivo `.env.`
- Ejecuta los comandos para crear la base de datos `php artisan migrate && php artisan db:seed`
- Ejecuta el comando `npm install && npm run build` para incorporar las librerías de JS y CSS.

## Para ejecutar el proyecto en ambiente de desarrollo
- En la línea de comandos y desde el directorio del proyecto ejecuta `npm run dev` 
- En la línea de comandos y desde el directorio del proyecto levanta el servidor de desarrollo en un puerto libre. Ejemplo: `php artisan serve --port=8000`
- Abre el URL en el navegador http://127.0.0.1:8000/

## Cuentas de acceso
Las cuentas de acceso están en el seeder de la base de datos del proyecto.
